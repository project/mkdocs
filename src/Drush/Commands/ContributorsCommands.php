<?php

namespace Drupal\mkdocs\Drush\Commands;

use Drupal\Core\Utility\Token;
use Drupal\mkdocs\MkdocsHttpClientInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class ContributorsCommands extends DrushCommands {

  /**
   * Constructs a ContributorsCommands object.
   */
  public function __construct(
    private readonly Token $token,
    private readonly MkdocsHttpClientInterface $httpClient,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token'),
      $container->get('mkdocs.http_client'),
    );
  }

  /**
   * Generate maintainers list.
   */
  #[CLI\Command(name: 'mkdocs:contributors', aliases: ['mkdc'])]
  #[CLI\Argument(name: 'project_name', description: 'The project name.')]
  #[CLI\Option(name: 'project-type', description: 'The project type, e.g. core, module, theme, theme_engine, distribution, general')]
  #[CLI\Usage(name: 'mkdocs:contributors project', description: 'Generate a contributors markdown.')]
  public function generateMaintainers($project_name, $options = ['project-type' => 'module']) {
    $project_type = $options['project-type'];
    $project = $this->httpClient->getProject($project_name, $project_type);
    $contributors = $this->httpClient->getContributors();

    $folder = \Drupal::service('extension.list.module')->getPath('mkdocs') . '/templates';
    $loader = new FilesystemLoader($folder);
    $twig = new Environment($loader, [
      \Drupal::service('file_system')->getTempDirectory(),
    ]);
    $twig->addExtension(new DebugExtension());

    echo $twig->render('contributors.md.twig', [
      'project' => $project,
      'contributors' => $contributors,
    ]);
  }

}
