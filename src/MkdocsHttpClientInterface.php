<?php

declare(strict_types = 1);

namespace Drupal\mkdocs;

/**
 * A HTTP Client class.
 */
interface MkdocsHttpClientInterface {}
