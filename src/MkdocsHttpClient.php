<?php

declare(strict_types = 1);

namespace Drupal\mkdocs;

use Psr\Http\Client\ClientInterface;

/**
 * MkDocs HTTP Client.
 */
final class MkdocsHttpClient implements MkdocsHttpClientInterface {

  /**
   * Constructs a MkdocsHttpClient object.
   */
  public function __construct(
    private readonly ClientInterface $httpClient,
  ) {}

  /**
   * Gets the project.
   */
  public function getProject(string $project_name, $project_type = 'module') {
    $url = 'https://www.drupal.org/api-d7/node.json?type=project_' . $project_type . '&field_project_machine_name=' . $project_name;
    try {
      $request = $this->httpClient->request('GET', $url);
      $json = $request->getBody()->getContents();
      $data = json_decode($json, TRUE);
      return (array) $data['list'][0];
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Gets the maintainers.
   */
  public function getMaintainers(string $project_name) {
    $url = 'https://www.drupal.org/project/' . $project_name . '/maintainers.json';
    try {
      $request = $this->httpClient->request('GET', $url);
      $json = $request->getBody()->getContents();
      $data = json_decode($json, TRUE);
      foreach ($data as $uid => $account) {
        $account['account'] = $this->getUser($account['name']);
        $data[$uid] = $account;
      }
      return $data;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Gets the releases.
   */
  public function getReleases(string $project_id) {
    $rows = $data = [];
    $data['next'] = 'https://www.drupal.org/api-d7/node.json?type=project_release&field_release_project=' . $project_id . '&sort=created&direction=DESC';
    try {
      $i = 0;
      while (@$data['next']) {
        $data['next'] = str_replace('/node?', '/node.json?', $data['next']);
        $request = $this->httpClient->request('GET', $data['next']);
        $json = $request->getBody()->getContents();
        $data = json_decode($json, TRUE);
        $rows = array_merge($rows, $data['list']);
      }
      return (array) $rows;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Gets a user.
   */
  public function getUser(string $username) {
    $url = 'https://www.drupal.org/api-d7/user.json?name=' . $username;
    try {
      $request = $this->httpClient->request('GET', $url);
      $json = $request->getBody()->getContents();
      $data = json_decode($json, TRUE);
      return (array) $data['list'][0];
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Gets the contributors.
   */
  public function getContributors() {
    exec('git log --pretty="%an %ae%n%cn %ce" | sort -u', $output);
    return $output;
  }

}
